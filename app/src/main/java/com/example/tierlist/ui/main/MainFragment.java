package com.example.tierlist.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tierlist.CharListAdapter;
import com.example.tierlist.CharacterTier;
import com.example.tierlist.DetailCharacter;
import com.example.tierlist.R;
import com.example.tierlist.ui.detail.DetailCharacterFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private View view;

    private ListView lsVw_S;
    private ListView lsVw_A;
    private ListView lsVw_B;
    private ListView lsVw_C;
    private ListView lsVw_D;
    private ListView lsVw_E;

    private ImageButton imgBtn_S;
    private ImageButton imgBtn_A;
    private ImageButton imgBtn_B;
    private ImageButton imgBtn_C;
    private ImageButton imgBtn_D;
    private ImageButton imgBtn_E;

    private ArrayList<CharacterTier> allCharacters;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);

        finViews();

        addCharButton();

        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null){
            refillListView();
        }


        return view;
    }

    private void refillListView() {

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference characters = uid.child("characters");

        allCharacters = new ArrayList<>();

        characters.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                allCharacters.clear();

                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    CharacterTier characterTier = dataSnapshot.getValue(CharacterTier.class);
                    allCharacters.add(characterTier);
                }
                orderTierCharacters(allCharacters);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void orderTierCharacters(ArrayList<CharacterTier> allCharacters) {
        ArrayList<CharacterTier> sChar = new ArrayList<>();
        ArrayList<CharacterTier> aChar = new ArrayList<>();
        ArrayList<CharacterTier> bChar = new ArrayList<>();
        ArrayList<CharacterTier> cChar = new ArrayList<>();
        ArrayList<CharacterTier> dChar = new ArrayList<>();
        ArrayList<CharacterTier> eChar = new ArrayList<>();

        for (int i = 0; i < allCharacters.size(); i++) {
            switch (allCharacters.get(i).getTier()){
                case "S":
                    sChar.add(allCharacters.get(i));
                    break;
                case "A":
                    aChar.add(allCharacters.get(i));
                    break;
                case "B":
                    bChar.add(allCharacters.get(i));
                    break;
                case "C":
                    cChar.add(allCharacters.get(i));
                    break;
                case "D":
                    dChar.add(allCharacters.get(i));
                    break;
                case "E":
                    eChar.add(allCharacters.get(i));
                    break;
            }
        }

        setAdapterFinal(sChar, lsVw_S);
        setAdapterFinal(aChar, lsVw_A);
        setAdapterFinal(bChar, lsVw_B);
        setAdapterFinal(cChar, lsVw_C);
        setAdapterFinal(dChar, lsVw_D);
        setAdapterFinal(eChar, lsVw_E);


    }

    private void setAdapterFinal(ArrayList<CharacterTier> listChar, ListView lsVw) {

        CharListAdapter charListAdapter = new CharListAdapter(
                getContext(),
                R.layout.lv_character,
                listChar
        );
        lsVw.setAdapter(charListAdapter);

        ViewGroup.LayoutParams params = lsVw.getLayoutParams();
        int cardHeigh = 260;
        int listHeigh = 0;
        for (int i = 0; i < listChar.size()+1; i++) {
            listHeigh = listHeigh + cardHeigh;
        }
        params.height = listHeigh;
        lsVw.setLayoutParams(params);

        lsVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CharacterTier characterTier = (CharacterTier) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(getContext(), DetailCharacter.class);
                intent.putExtra("characterTier", characterTier);
                startActivity(intent);
            }
        });

    }

    // Find Elements inside Layout
    private void finViews() {
        lsVw_S = view.findViewById(R.id.lsVw_S);
        lsVw_A = view.findViewById(R.id.lsVw_A);
        lsVw_B = view.findViewById(R.id.lsVw_B);
        lsVw_C = view.findViewById(R.id.lsVw_C);
        lsVw_D = view.findViewById(R.id.lsVw_D);
        lsVw_E = view.findViewById(R.id.lsVw_E);

        imgBtn_S = view.findViewById(R.id.imgBtn_S);
        imgBtn_A = view.findViewById(R.id.imgBtn_A);
        imgBtn_B = view.findViewById(R.id.imgBtn_B);
        imgBtn_C = view.findViewById(R.id.imgBtn_C);
        imgBtn_D = view.findViewById(R.id.imgBtn_D);
        imgBtn_E = view.findViewById(R.id.imgBtn_E);
    }

    // Buttons per Tier
    private void addCharButton() {
        imgBtn_S.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCharPopup("S");
            }
        });
        imgBtn_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCharPopup("A");
            }
        });
        imgBtn_B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCharPopup("B");
            }
        });
        imgBtn_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCharPopup("C");
            }
        });
        imgBtn_D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCharPopup("D");
            }
        });
        imgBtn_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCharPopup("E");
            }
        });
    }

    // Popup Add Character per Tier
    private void createCharPopup(String tier) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.getContext());

        final View popUpView = getLayoutInflater().inflate(R.layout.popup_newcharacter, null);

        TextInputEditText editText_nameChar = popUpView.findViewById(R.id.editText_nameChar);
        TextInputEditText editText_imageChar = popUpView.findViewById(R.id.editText_imageChar);
        Button btn_addChar = popUpView.findViewById(R.id.btn_addChar);
        TextView txtVw_tierNew = popUpView.findViewById(R.id.txtVw_tierNew);

        txtVw_tierNew.setText(tier);

        // PUP UP DIALOG BUILDER
        dialogBuilder.setView(popUpView);
        AlertDialog dialog = dialogBuilder.show();
        dialog.show();

        btn_addChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editText_nameChar.getText().toString().isEmpty()){

                    CharacterTier characterTier = new CharacterTier(editText_nameChar.getText().toString(), editText_imageChar.getText().toString(), tier);

                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    DatabaseReference base = FirebaseDatabase.getInstance().getReference();

                    DatabaseReference users = base.child("users");
                    DatabaseReference uid = users.child(auth.getUid());
                    DatabaseReference characters = uid.child("characters");

                    DatabaseReference reference = characters.push();
                    reference.setValue(characterTier);

                }
                else {
                    CharSequence text = "Envío de datos Fallido";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}