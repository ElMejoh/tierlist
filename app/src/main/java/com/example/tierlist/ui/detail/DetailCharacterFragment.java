package com.example.tierlist.ui.detail;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tierlist.CharacterTier;
import com.example.tierlist.DetailCharacter;
import com.example.tierlist.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

public class DetailCharacterFragment extends Fragment {

    private DetailCharacterViewModel mViewModel;
    private View view;

    private TextView detailName;
    private ImageView detailImage;
    private ImageView audioPlay;
    private FloatingActionButton fltActbtn_startRecord;
    private TextView counterTime;

    private String count;
    private int auxCount;
    private boolean timeRun = false;

    private String fileName;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;


    public static DetailCharacterFragment newInstance() {
        return new DetailCharacterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.detail_character_fragment, container, false);

        Intent i = getActivity().getIntent();

        ActivityCompat.requestPermissions(getActivity(), DetailCharacter.permissions, DetailCharacter.REQUEST_RECORD_AUDIO_PERMISSION);
        String ruta = getContext().getExternalFilesDir(null).getAbsolutePath();
        fileName = ruta + "/audiorecord.3gp";


        if (i != null){
            CharacterTier characterTier = (CharacterTier) i.getSerializableExtra("characterTier");

            if (characterTier != null){
                cargarFragment(characterTier, view);
            }
        }

        return view;
    }

    private void cargarFragment(CharacterTier characterT, View view) {

        detailName = view.findViewById(R.id.txtVw_detailName);
        detailImage = view.findViewById(R.id.imgVw_detailImage);
        audioPlay = view.findViewById(R.id.imgVw_playRecord);
        fltActbtn_startRecord = view.findViewById(R.id.fltActbtn_startRecord);
        counterTime = view.findViewById(R.id.txtVw_counterTime);


        detailName.setText(characterT.getName());
        Glide.with(getContext())
                .load(characterT.getImageURL())
                .into(detailImage);

        audioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!timeRun){
                    playRecording();
                }
            }
        });

        fltActbtn_startRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!timeRun){
                    timeRun = true;
                    recordingAudio(counterTime, audioPlay);
                }
            }
        });

    }



    private void recordingAudio(TextView counterTime, ImageView audioPlay) {
        audioPlay.setVisibility(ImageView.INVISIBLE);
        count = "6";
        auxCount = Integer.parseInt(count);
        long valor = auxCount*1000;
        counterTime.setText(count);
        counterTime.setVisibility(TextView.VISIBLE);

        startRecording();

        CountDownTimer cuenta = new CountDownTimer(valor,1000) {
            @Override
            public void onTick(long l) {
                auxCount--;
                count = String.valueOf(auxCount);
                counterTime.setText(count);
            }

            @Override
            public void onFinish() {
                counterTime.setVisibility(TextView.INVISIBLE);
                audioPlay.setVisibility(ImageView.VISIBLE);
                timeRun = false;

                stopRecording();
            }
        }.start();

    }



    private void startRecording() {
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(fileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Log.e("RECORDING", "No es pot iniciar la gravació");
            }
            mRecorder.start();
        }
    }

    private void stopRecording() {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.reset();
            mRecorder.release();
            mRecorder = null;
        } else if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    private void playRecording(){

        if (mRecorder == null && mPlayer == null){
            mPlayer = new MediaPlayer();

            try {
                mPlayer.setDataSource(fileName);
                mPlayer.prepare();
                mPlayer.start();

                mPlayer.setOnCompletionListener(mediaPlayer -> {
                   mPlayer.stop();
                });
            } catch (IOException e) {
                Log.e("RECORDING", "No es pot iniciar la reproducció");
            }
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailCharacterViewModel.class);
        // TODO: Use the ViewModel
    }

}