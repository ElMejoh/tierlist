package com.example.tierlist;

import java.io.Serializable;

public class CharacterTier implements Serializable {

    private String name;        // NAME
    private String imageURL;    // IMAGEN
    private String tier;

    // CONSTRUCTOR
    public CharacterTier(String name, String imageURL, String tier) {
        this.name = name;
        this.imageURL = imageURL;
        this.tier = tier;
    }

    public CharacterTier(){

    }

    // GETTERS
    public String getName() { return name; }
    public String getImageURL() { return imageURL; }
    public String getTier() { return tier; }

    // SETTERS
    public void setName(String name) { this.name = name; }
    public void setImageURL(String imageURL) { this.imageURL = imageURL; }
    public void setTier(String tier) { this.tier = tier; }

}
