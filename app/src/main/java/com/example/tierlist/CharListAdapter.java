package com.example.tierlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CharListAdapter extends ArrayAdapter<CharacterTier> {


    public CharListAdapter(@NonNull Context context, int resource, ArrayList<CharacterTier> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        CharacterTier character = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_character, parent, false);
        }

        TextView name = convertView.findViewById(R.id.txtVw_characterName);
        ImageView image = convertView.findViewById(R.id.imgVw_characterImage);

        name.setText(character.getName());

        if (character.getImageURL().isEmpty() || character.getImageURL() == null){
            Glide.with(getContext())
                    .load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGxyWuRUC1WNfeRK5I5SfDKIyr-4bqp64dsg&usqp=CAU")
                    .into(image);
        }
        else {
            Glide.with(getContext())
                    .load(character.getImageURL())
                    .into(image);
        }





        return convertView;
    }
}
